time: 15:00
[original-repo...](https://gitlab.com/dev-coach/myapp/-/blob/master/back/public/worker.php)


Инструкции командной строки
Вы также можете загрузить существующие файлы с компьютера, используя приведённые ниже инструкции.


Глобальные настройки Git
git config --global user.name "Андрей"
git config --global user.email "splaandrey@gmail.com"

Создать новый репозиторий
git clone git@gitlab.com:splaa/myapp.git
cd myapp
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Отправить существующую папку
cd existing_folder
git init
git remote add origin git@gitlab.com:splaa/myapp.git
git add .
git commit -m "Initial commit"
git push -u origin master

Отправить существующий репозиторий Git
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:splaa/myapp.git
git push -u origin --all
git push -u origin --tags